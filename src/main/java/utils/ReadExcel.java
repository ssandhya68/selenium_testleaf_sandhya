package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String filename) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		
		int lastRowNum = sheet.getLastRowNum();
		System.out.println("lastRowNum: "+lastRowNum);
		
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		System.out.println("lastCellNum: "+lastCellNum);
		
		Object[][] data = new Object[lastRowNum][lastCellNum];
		
		for(int j=1;j<=lastRowNum;j++) {
			XSSFRow row = sheet.getRow(j);
			
			for (int i=0;i<lastCellNum;i++) {
				XSSFCell cell = row.getCell(i);
				data[j-1][i]=cell.getStringCellValue();
			}
		}
		return data;

	}

}
