package week0;

import java.util.Scanner;

public class ConditionalOperators {

	public static void main(String[] args) {
		System.out.println("Enter the Input");

		Scanner scan= new Scanner(System.in);

		int n1,n2;

		n1=scan.nextInt();
		n2=scan.nextInt();

		System.out.println("value of n1 is:" +n1);
		System.out.println("value of n2 is:" +n2);
		
		if(n1==n2)
		{
		System.out.println("n1 is equal to n2"); //Relational Operators are used in this code.
		}
		else
		
			System.out.println("n1 is not equal to n2");
		
		if(n1!=n2)
		{
		System.out.println("n1 is equal to n2");
		}
		else
		
			System.out.println("n1 is not equal to n2");
		
		if(n1>n2)
		{
		System.out.println("n1 is greater than n2");
		}
		else
		
			System.out.println("n1 is not greater than n2");
		
		if(n1<n2)
		{
		System.out.println("n1 is lesser than n2");
		}
		else
		
			System.out.println("n1 is not lesser than n2");
		
		if(n1>=n2)
		{
		System.out.println("n1 is greater than or equal to n2");
		}
		else
		
			System.out.println("n1 is not greater than or equal to n2");
		
		if(n1<=n2)
		{
		System.out.println("n1 is lesser than or equal to n2");
		}
		else
		
			System.out.println("n1 is not lesser than or equal to n2");
		
		if((n1==1) && (n2==2))
			System.out.println("n1 is 1 AND n2 is 2");
		
		if((n1==1) || (n2==2))
			System.out.println("n1 is 1 OR n2 is 2");

		
		

	}

}
