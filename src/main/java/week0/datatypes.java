package week0;

public class datatypes {

	public static void main(String[] args) {
		boolean b = true;  //size: 1-bit Description: true or false
		if(b==true)
			System.out.println("Hi Geek");
		
		char z='S';   //size: 16-bit 
		
		byte a=127;   //size: 8-bit value: -128 to 127
		
		short c=32767;   //size: 16-bit value: -32768 to 32767
		
		int i=89;// size: 32-bit value: -2^31 to 2^31-1 
		
		long l=234567890;  //size: 64-bit value: -2^63 to 2^63-1
		
		float f=14.245f; //size: 32-bits suffix: F/f Example: 9.8f
		
		double d=4.2345678d; //size: 64-bits 
		
		System.out.println("char:" +z);
		System.out.println("byte:" +a);
		System.out.println("short:" +c);
		System.out.println("int:" +i);
		System.out.println("long:" +l);
		System.out.println("float:" +f);
		System.out.println("double:" +d);
		
	
		
			

	}

}
