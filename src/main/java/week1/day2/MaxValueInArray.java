package week1.day2;

import java.util.Scanner;

public class MaxValueInArray {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number of elements in an array:");
		int n=scan.nextInt();
		int max;
		int a[]=new int[n];
		System.out.println("Enter the numbers");
		for(int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}
		max=a[0];
				for(int i=0;i<n;i++)
				{
					if(max<a[i])
					{
						max=a[i];
					}
				}
				System.out.println("Maximum value" +max);
	}

}
