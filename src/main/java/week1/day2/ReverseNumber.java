package week1.day2;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		int number,rem,reverse;
		rem=0;
		reverse=0;
		System.out.println("Enter the Number:");
		Scanner scan= new Scanner(System.in);
		number=scan.nextInt();
		while(number>0)
		{
			rem=number%10;
			reverse=(reverse*10)+rem;
			number=number/10;
			}
		System.out.println("The Reverse for given number is:" +reverse);
	}

}
