package week1.day2;

import java.util.Scanner;

public class MonthDays {

	public static void main(String[] args) {
		System.out.println("Enter the number");
		Scanner scan=new Scanner(System.in);
		int month=scan.nextInt();
		
		switch(month)
		{
		case 1:
		{
			System.out.println("The name of month number 1 is January");
			System.out.println("31 days in this month");
			break;
		}
		case 3:
		{
			System.out.println("The name of month number 3 is March");
			System.out.println("31 days in this month");
			break;
		}
		case 5:
		{
			System.out.println("The name of month number 5 is May");
			System.out.println("31 days in this month");
			break;
		}
		case 7:
		{
			System.out.println("The name of month number 7 is July");
			System.out.println("31 days in this month");
			break;
		}
		case 8:
		{
			System.out.println("The name of month number 8 is August");
			System.out.println("31 days in this month");
			break;
		}
		case 10:
		{
			System.out.println("The name of month number 10 is October");
			System.out.println("31 days in this month");
			break;
		}
		case 11:
		{
			System.out.println("The name of month number 11 is November");
			System.out.println("31 days in this month");
			break;
		}
		case 12:
		{
			System.out.println("The name of month number 12 is December");
			System.out.println("31 days in this month");
			break;
		}
			
		case 4:
		{
			System.out.println("The name of month number 4 is April");
			System.out.println("30 days in this month");
			break;
		}
		case 6:
		{
			System.out.println("The name of month number 6 is June");
			System.out.println("30 days in this month");
			break;
		}
		case 9:
		{
			System.out.println("The name of month number 9 is September");
			System.out.println("30 days in this month");
			break;
		}
		case 2:
		{
			System.out.println("The name of month number 2 is February");
			System.out.println("28 or 29 days in this month");
			break;
		}
			
		default:
			System.out.println("Enter the number");	

		}

	}

}
