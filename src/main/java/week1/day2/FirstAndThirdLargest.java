package week1.day2;

import java.util.Scanner;

public class FirstAndThirdLargest {

	public static void main(String[] args) {
		int n,temp;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number of elements in an array");
		n=scan.nextInt();
		int a[]=new int[n];
		System.out.println("Enter the numbers");
		for(int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}
		
		for(int i=0;i<n;i++)
		{
			for(int j=i+1;j<n;j++)
			{
				if(a[i]<a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		
		System.out.println("The First Largest Number is:" +a[0]);
		System.out.println("The Third Largest Number is:" +a[2]);
	}

}
