package week1.day2;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		System.out.println("Enter the value of n:");
		Scanner scan= new Scanner(System.in);
		int n=scan.nextInt();
		int f1=0;
		int f2=1;
		int sum=f1+f2;
		System.out.println(f1);
		while(sum<=n)
		{
			System.out.println(sum);
			sum=f1+f2;
			f1=f2;
			f2=sum;
		}
	}

}
