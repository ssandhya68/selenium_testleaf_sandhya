package week1.day1;

public class OddNumbers {

	public static void main(String[] args) {
		System.out.println("The odd Numbers between 50 to 100 are:");
		int sum=0;
		for(int i=50;i<=100;i++)
		{
			if(i%2!=0)
			{
				System.out.println(i);
				sum=sum+i;
			}
		}
		System.out.println("The sum of odd Numbers between 50 to 100 are:" +sum);
	}

}
