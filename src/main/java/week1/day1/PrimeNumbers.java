package week1.day1;

public class PrimeNumbers {
	// 2 factor nos like 2=1*2, 3=1*3 are prime nos. 
	// more than 2 factors like 6 =1*6 or 2*3, 8=1*8 or 2*4 are composite nos.
	public static void main(String[] args) {
		int i=0;
		int number=0;
		String primeNumbers="";
		
		for(i=1;i<=20;i++)
		{
			int count=0;
			for(number=i;number>=1;number--)
			{
				if(i%number==0)
				{
					count++;
		
			}
		}
		if(count==2)
		{

			primeNumbers=primeNumbers + i + "";
		}
		}
		System.out.println("Prime numbers from 1 to 20 are:");
		System.out.println(primeNumbers);

	}

}
