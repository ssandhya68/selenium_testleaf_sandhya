package Week2;

public class MyPhone {
	// Telecom -- MobilePhone extends TeleCom
	//--Samsung extends MobilePhone--MyPhone object 's' created.
	//In Samsung OVERRIDES week2.MobilePhone.Games
	//In MyPhone calling methods from Samsung, TeleCom and MobilePhone.
	public static void main(String[] args) {

		Samsung s = new Samsung();
		
		System.out.println("Games: " +s.Games());
		System.out.println("calculator: " +s.Calculator());
		System.out.println("Dialler: "+s.Dialler());
		System.out.println("SMS: " +s.SMS());
		
		MobilePhone mp=new MobilePhone();
		System.out.println("Number: " +mp.dialNumber());
	}

}
