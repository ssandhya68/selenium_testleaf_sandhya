package Week2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		List<String> myPhones = new ArrayList<String>();
		myPhones.add("nokia");
		myPhones.add("Xolo");
		myPhones.add("Samsung");
		myPhones.add("Vivo");
		myPhones.add("Samsung");
		int size=myPhones.size();
		System.out.println("Total Number of Items:" +size);
		
		System.out.println("Last but one Mobile Phone:" +myPhones.get(size-2));
		
		Collections.sort(myPhones);
		System.out.println("ASCII order:");
		
		for(String eachPhone:myPhones)
		
			System.out.println(eachPhone);
		
		Collections.reverse(myPhones);
		System.out.println("ASCII order in reverse order:");
		
		for(String eachPhone:myPhones)
		
			System.out.println(eachPhone);
		
		
		
		

	}

}
