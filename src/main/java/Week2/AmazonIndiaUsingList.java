package Week2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class AmazonIndiaUsingList {

	public static void main(String[] args) {

		String name="Amazon India";
		List<Character> ch = new ArrayList<Character>();

		char[] allChar=name.toCharArray();
		for(char c:allChar)
		{
			ch.add(c);

		}
		Collections.sort(ch);
		System.out.println("Printing the sorted order:");

		for(char sort : ch)
		{

			System.out.println(sort);

		}

		ch.clear();

		String uCase=name.toUpperCase();
		char[] uChar=uCase.toCharArray();
		for(char d:uChar)
		{
			ch.add(d);
		}

		Collections.sort(ch);
		Collections.reverse(ch);
		System.out.println("Printing the reverse sorted order:");
		for(char rev:ch)
		{
			System.out.println(rev);
		}
		Set <Character> dup=new HashSet<>();
		dup.addAll(ch);
		ch.clear();
		ch.addAll(dup);
		Collections.sort(ch);
		System.out.println("Removed duplicates and Printing in sorted order:");
		for(char g : ch)
		{
			System.out.println(g);
		}
	}}
