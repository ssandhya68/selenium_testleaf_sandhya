package Week2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnSet {

	public static void main(String[] args) {
		
Set<String> Mobiles=new LinkedHashSet<String>(); //We can try using HashSet and TreeSet as well.
Mobiles.add("Nokia");
Mobiles.add("Moto");
Mobiles.add("Samsung");
Mobiles.add("IPhone");
Mobiles.add("Samsung");

for(String eachMobile:Mobiles)
{
	System.out.println(eachMobile);
}

List<String> lst=new ArrayList<String>(); 
//'get' not possible bcz of Random Algorithm 
//hence we are creating a list and adding 'get' within the list.
lst.addAll(Mobiles);
System.out.println("First Mobile in the List:");
String str=lst.get(0);
System.out.println(str);
	}

}
