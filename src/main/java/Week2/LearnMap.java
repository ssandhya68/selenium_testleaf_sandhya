package Week2;

import java.util.LinkedHashMap;
import java.util.Map;

public class LearnMap {

	public static void main(String[] args) {
		String Name="Sandhya";
		
		Map<Character,Integer> map=new LinkedHashMap <Character,Integer>();
		
		char[] charArray = Name.toCharArray(); 
		
		for(char c:charArray)
		{
		if(map.containsKey(c))  //This is to verify duplicate character in String Name "Sandhya"
			
		{
		System.out.println(c + " is duplicate");
		
			}
		
		else
		{
			map.put(c,1);  //will give the value as 1 for each character in the String "Sandhya"
		}

		}	
	
	}
}

