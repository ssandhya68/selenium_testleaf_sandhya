package wdMethods_old;

import org.openqa.selenium.WebElement;

public class projectMethods extends SeMethods {
	
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);	
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);	
			
	}
}
