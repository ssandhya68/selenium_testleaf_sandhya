package projectday;



	import java.util.ArrayList;
	import java.util.Collections;
	import java.util.List;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.interactions.Actions;
	import org.testng.annotations.Test;

	public class FixTheBug {

		@Test
		//main(String args) ---- removed main method under Test annotation
		public void BugFix() throws InterruptedException {

			// launch the browser
			//Webdriver.Chrome.Driver---replaced with lowercase "webdriver.chrome.driver"
			System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.myntra.com/");

			// Mouse Over on Men
			Actions builder = new Actions(driver);
			builder.moveToElement(driver.findElementByLinkText("Men")).perform();
			Thread.sleep(5000);
			
			// Click on Jackets
			driver.findElementByXPath("//a[@href='/men-jackets']").click();


			// Find the count of Jackets
			//Bug found following:sibling ----replaced by --- following-sibling
			//replaceAll("//D", "") is replaced with replaceAll("\\D", "")
			String leftCount = 
					driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span")
					.getText()
					.replaceAll("\\D", "");
			System.out.println(leftCount);


			// Click on Jackets and confirm the count is same
			driver.findElementByXPath("//label[text()='Jackets']").click();

			// Wait for some time
			Thread.sleep(5000);

			// Check the count
			//Bug found following:sibling ----replaced by --- following-sibling
			//replaceAll("//D", "") is replaced with replaceAll("\\D", "")
			String rightCount = 
					driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
					.getText()
					.replaceAll("\\D", "");
			System.out.println(rightCount);

			// If both count matches, say success
			if(leftCount.equals(rightCount)) {
				System.out.println("The count matches on either case");
			}else {
				System.err.println("The count does not match");
			}

			// Click on Offers
			//Bug Found --- Offers is not there in the webpage.
			//driver.findElementByXPath("//h3[text()='Offers']").click();

			// Find the costliest Jacket
			//Bug found replaced replaceAll("//D", "") with replaceAll("\\D", "")
			//converted string to integer
			
			List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
			List<Integer> onlyPrice = new ArrayList<Integer>();

			for (WebElement productPrice : productsPrice) {
				String rp1 = productPrice.getText().replaceAll("\\D", "");
				int rp2 = Integer.parseInt(rp1);
				onlyPrice.add(rp2);
			}

			// Sort them 
			int max = Collections.max(onlyPrice);

			// Find the top one
			System.out.println("costliest Jacket");
			System.out.println(max);
			
			//Bug found --- commented below --- no need to close browser here
			//driver.close();

			// Print Only Allen Solly Brand Minimum Price
			//Bug Found following:sibling::div is replaced with following-sibling::div
			//added below to click +195 more
			//WebElement wb1 = driver.findElementByXPath("//div[@class='brand-more']");
			//wb1.click();
			WebElement wb1 = driver.findElementByXPath("//div[@class='brand-more']");
			wb1.click();
			driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();
			Thread.sleep(3000);
			driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']");
			Thread.sleep(1000);

			// Find the costliest Jacket
			List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

			List<Integer> onlyPrice1 = new ArrayList<Integer>();

			for (WebElement productPrice1 : allenSollyPrices) {
				String rp3=productPrice1.getText().replaceAll("\\D", "");
				int rp4=Integer.parseInt(rp3);
				onlyPrice1.add(rp4);
				
			}

			// Get the minimum Price 
			int min = Collections.min(onlyPrice1);

			// Find the lowest priced Allen Solly
			System.out.println("Minimum Price Allen Solly Brand");
			System.out.println(min);


		}

	}

	


