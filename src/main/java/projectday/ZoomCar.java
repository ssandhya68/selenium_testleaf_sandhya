package projectday;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class ZoomCar extends ProjectMethods{
	
	@BeforeTest
	public void Car() {
		testCaseName="TC002_Zoomcar";
		testCaseDesc="Wonderful Journey";
		category="smoke";
		author="Sandhya";
				
	}
	
	@BeforeMethod
	public void login() {
	startApp("chrome", "https://www.zoomcar.com/chennai");
	}
	
	@Test
	public void ZoomCar1()
	{
		WebElement journey = locateElement("linkText","Start your wonderful journey");
		click(journey);
		WebElement pickuppoint = locateElement("xpath","(//div[@class=\"component-popular-locations\"]/*[4])");
		click(pickuppoint);
		WebElement next = locateElement("class","proceed");
		click(next);
		Date date2 = new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date2);
		int tomorrow=Integer.parseInt(today)+1;
		
		WebElement nextday = locateElement("xpath","(//div[text()[contains(.,'"+tomorrow+"')]])");
		click(nextday);
		next = locateElement("class","proceed");
		click(next);
		next = locateElement("class","proceed");
		click(next);
		List<WebElement> Result = driver.findElementsByXPath("//div[@class='component-car-item']");
		int size = Result.size();
		System.out.println("The Number of Cars" +size);
		
		List<WebElement> Price = driver.findElementsByClassName("price");
		List<Integer> allPrices = new ArrayList<Integer>();
		for (WebElement eachPrice : Price) {
			allPrices.add(Integer.parseInt(eachPrice.getText().replaceAll("\\D", "")));
		}
		int maxValue = Collections.max(allPrices);
		WebElement MaxBrand = locateElement("xpath","(//div[@class='car-book']//div[contains(text(), '"+maxValue+"')]/preceding::div[@class='details'][1]/h3)");
		String text = getText(MaxBrand);
		System.out.println(text);
		WebElement bookNow = locateElement("xpath","(//div[@class='car-book']//div[contains(text(), '"+maxValue+"')]/following-sibling::button)");
		click(bookNow);
		
	}
	

	}


