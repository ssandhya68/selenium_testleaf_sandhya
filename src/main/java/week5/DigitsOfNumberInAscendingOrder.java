package week5;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class DigitsOfNumberInAscendingOrder {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Input Number");
		int n1= s.nextInt();
		Set<Integer> no = new LinkedHashSet<Integer>();
		
		while(n1>0)
		{
			int digit=n1%10;
			n1=n1/10;
			no.add(digit);
			}
		List<Integer> list=new LinkedList<>();
		list.addAll(no);
		Collections.sort(list);
		int size = list.size();
		int number=0;
		for(int i=0;i<size;i++) {
			
			number=number*10+(list.get(i));
			}
		System.out.println("Output: "+number);

	}

}
