package week5;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_createLead","Create a new Lead in Leaftaps");
		test.assignCategory("Smoke");
		test.assignAuthor("Sandhya");
		test.pass("Browser launched successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.fail("The data DemoSalesManager not entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("The data crmsfa entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("The element login button clicked successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		extent.flush();

	}

}
