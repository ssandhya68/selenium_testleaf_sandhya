package week5;

import java.util.Scanner;

public class ReverseStringandNumberUsingRecursion {
	private static Scanner sc;   //to Reverse a number

	public static void main(String[] args) {
		String str;
		System.out.println("Enter the String:");
		Scanner scan = new Scanner(System.in);
		str=scan.nextLine();
		String reversed = reverseString(str);
        System.out.println("The reversed string is: " + reversed);
        
        //Below method to reverse a number
        int Number, Reminder, Reverse = 0;
		 sc = new Scanner(System.in);
		
		System.out.println("\n Please Enter any Number you want to Reverse : ");
		Number = sc.nextInt();
		
		while(Number > 0) {
			Reminder = Number %10;
			Reverse = Reverse * 10 + Reminder;
			Number = Number /10;
		}
		System.out.format("\n Reverse of entered number is = %d\n", Reverse);
	}

	

	private static String reverseString(String str) {
		if (str.isEmpty())
            return str;
        //Calling Function Recursively
        return reverseString(str.substring(1)) + str.charAt(0);

	}

}
