package testcase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLead;
import pages.LoginPage;
import pages.MyLeads;
import wdMethods.ProjectMethods;
import wdMethods.ProjectMethods2;

public class TestCase008 extends ProjectMethods2{
	@BeforeTest
	public void setData() {
		testCaseName="TC008_LoginAndCreateLead";
		testCaseDesc="To Login And Create Lead";
		category="Smoke";
		author="Sandhya";
		ExcelFileName="Login_CreateLead";
	}
	
	
	@Test(dataProvider="FetchData")
public void Login(String uName,String pwd,String vUserName,String CompanyName,String FirstName,String LastName,String vCompanyName,String vFirstName,String vLastName)
	
{
	new LoginPage().typeUserName(uName)	
	.typePassword(pwd)
	.clickLogin()
	.verifyLoggedInName(vUserName)
	.clickCRM()
	.clickLeads().clickCreateLeads()
	.typeCompanyName(CompanyName)
	.typeFirstName(FirstName)
	.typeLastName(LastName)
	.clickCreateLead()
	.verifyCompanyName(vCompanyName)
	.verifyFirstName(vFirstName)
	.verifyLastName(vLastName);
	
	
	}
	
}

