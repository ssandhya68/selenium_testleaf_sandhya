package testcase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="MergeLead";
		testCaseDesc="To Merge Lead";
		category="smoke";
		author="Sandhya";
				
		
	}

	@Test
	public void Merge()
	{
		WebElement leads = locateElement("linkText","Leads");
		click(leads);
		WebElement mergeleads = locateElement("linkText","Merge Leads");
		click(mergeleads);
		WebElement fromlead = locateElement("xpath","(//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a)");
		click(fromlead);
		Set<String> allwindows = driver.getWindowHandles();
		List<String> listofwindows = new ArrayList<String>();
		listofwindows.addAll(allwindows);
		String window = listofwindows.get(1);
		driver.switchTo().window(window);
		driver.manage().window().maximize();
		WebElement leadid = locateElement("name","id");
		type(leadid,"10269");
		WebElement findleads = locateElement("xpath","(//button[text()='Find Leads'])");
		click(findleads);
		WebElement firstOutput = locateElement("xpath","(((//tbody/tr/td[4])//preceding-sibling::td/div/a)[1])");
		click(firstOutput);
		String window1 = listofwindows.get(0);
		driver.switchTo().window(window1);
		
		WebElement tolead = locateElement("xpath","(//table[@id='widget_ComboBox_partyIdTo']//following-sibling::a)");
		click(tolead);
		Set<String> allwindows1 = driver.getWindowHandles();
		List<String> listofwindows1 = new ArrayList<String>();
		listofwindows1.addAll(allwindows1);
		String window2 = listofwindows1.get(1);
		driver.switchTo().window(window2);
		driver.manage().window().maximize();
		WebElement leadid1 = locateElement("name","id");
		type(leadid1,"10598");
		WebElement findleads1 = locateElement("xpath","(//button[text()='Find Leads'])");
		click(findleads1);
		WebElement firstOutput1 = locateElement("xpath","(((//tbody/tr/td[4])//preceding-sibling::td/div/a)[1])");
		click(firstOutput1);
		String window3 = listofwindows1.get(0);
		driver.switchTo().window(window3);
		WebElement merge = locateElement("class","buttonDangerous");
		click(merge);
		driver.switchTo().alert().accept();
		WebElement findLeads2 = locateElement("linkText", "Find Leads");
		click(findLeads2);
		WebElement Leadid2 = locateElement("name","id");
		type(Leadid2,"10269");
		WebElement findleads3 = locateElement("xpath","(//button[text()='Find Leads'])");
		click(findleads3);
		WebElement output = locateElement("class","x-paging-info");
		verifyExactText(output, "No records to display");
	}
	}

