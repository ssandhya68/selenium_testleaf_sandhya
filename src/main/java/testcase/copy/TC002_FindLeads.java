package testcase.copy;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods_old.SeMethods;

public class TC002_FindLeads extends SeMethods{
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("Id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
}
	public void FindLeads()
	{
		WebElement crm = locateElement("linktext","CRM/SFA");
		click(crm);
		WebElement Leads = locateElement("linktext","Leads");
		click(Leads);
		WebElement FindLeads = locateElement("linktext","Find Leads");
		click(FindLeads);
	}
}

