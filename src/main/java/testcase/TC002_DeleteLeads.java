package testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_DeleteLeads extends ProjectMethods {
	//@Before Suite will be invoked to set html report property.Common for all Test cases(predefined)
		//@Before Test will be invoked after to set variable value for @before class method to be added in reports
		//Common for all Test cases(predefined)
		@BeforeTest(groups= {"common"})//Common for all Test cases(predefined) differs for each test case need to be written 
		public void setData() {
			testCaseName="TC003_DeleteLead";
			testCaseDesc="Delete a Lead";
			category="Smoke";
			author="Sandhya";
}
		@Test(groups= {"sanity"})
		public void DeleteLead() {
			
			WebElement leads = locateElement("linkText", "Leads");
			click(leads);
			WebElement findLeads = locateElement("linkText", "Find Leads");
			click(findLeads);
			WebElement phone = locateElement("linkText", "Phone");
			click(phone);
			
	WebElement countryCode = locateElement("name", "phoneCountryCode");
	   countryCode.clear();

	    type(countryCode,"91");
			
			WebElement areaCode = locateElement("name", "phoneAreaCode");
			type(areaCode,"44");
			
			WebElement phoneNumber = locateElement("name", "phoneNumber");
			type(phoneNumber,"7708331227");
			WebElement find = locateElement("xpath", "(//tbody/tr/td/em)[7]");
		   click(find);
			WebElement firstOutput = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
			getText(firstOutput);
			String text = getText(firstOutput);
			System.out.println("The Value of first element is "+text);
			WebElement firstOutput1 = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
			click(firstOutput1);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement delete = locateElement("class", "subMenuButtonDangerous");
			click(delete);
			WebElement findlead = locateElement("linkText","Find Leads");
			click(findlead);
			
			WebElement leadId = locateElement("xpath","(//input[@name='id'])");
			type(leadId,text);
			WebElement findlead2 = locateElement("xpath", "(//tbody/tr/td/em)[7]");
			click(findlead2);
			WebElement output = locateElement("class","x-paging-info");
			verifyExactText(output, "No records to display");
		}
}