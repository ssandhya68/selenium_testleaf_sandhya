package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods2;

public class TestCase007 extends ProjectMethods2{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC007_LoginAndLogout";
		testCaseDesc="To Login";
		category="Smoke";
		author="Sandhya";
		ExcelFileName="Login";
	}
	
	
	@Test(dataProvider="FetchData")
public void Login(String uName,String pwd,String vUserName)	
{
	new LoginPage()
	.typeUserName(uName)	
	.typePassword(pwd)
	.clickLogin()
	.verifyLoggedInName(vUserName)
	.clickLogout();
	}
	
}



