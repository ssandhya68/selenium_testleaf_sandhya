package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class lastButOne {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/ChromeDriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		WebElement drop = driver.findElementByClassName("dropdown");
		Select dropDown=new Select(drop);
		List<WebElement> options = dropDown.getOptions();
		int size=options.size();
		dropDown.selectByIndex(size-2);

	}

}
