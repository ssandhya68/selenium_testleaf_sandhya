package week3.day1;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/ChromeDriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().window().maximize();
	
		List<WebElement> noOfCheckBoxes = driver.findElementsByXPath("//input[@type='checkbox']");
		System.out.println(noOfCheckBoxes.size());
		
	int s=noOfCheckBoxes.size();
	WebElement lastCheckBox = noOfCheckBoxes.get(s-1);
	lastCheckBox.click();
	WebElement cK = driver.findElementByXPath("//font[contains(text(),'80')]/following::input");
	cK.click();
	}

}
