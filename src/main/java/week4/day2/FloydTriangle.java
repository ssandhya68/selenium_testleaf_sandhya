package week4.day2;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter the number of rows");
int rowNum=scan.nextInt();
int num=1;
for(int i=1;i<=rowNum;i++)
{
	for(int j=1;j<=i;j++)
	{
		System.out.print(num+ " ");
		num++;
	}
	System.out.println();
}
	}

}
