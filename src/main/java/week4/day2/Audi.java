package week4.day2;

class Vehicle
{
	public Vehicle()
	{
		System.out.println("Vehicle");
	}
}

class Car extends Vehicle
{
	public Car()
	{
		System.out.println("Car");
	}
}
 public class Audi extends Car {

	public Audi()
	{
		System.out.println("Audi");
	}
	public static void main(String[] args) {
		
		new Audi();

	}

}
