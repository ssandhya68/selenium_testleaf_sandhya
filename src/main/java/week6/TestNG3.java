package week6;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TestNG3 extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC003_DuplicateLead";
		testCaseDesc="To get Lead by email";
		category="Smoke";
		author="Sandhya";
	}
	@Test(dataProvider="Positive")
	public void DuplicateLeads(String email1) throws InterruptedException {

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement findLeads = locateElement("linkText", "Find Leads");
		click(findLeads);
		WebElement email = locateElement("linkText", "Email");
		click(email);
		WebElement emailAddress = locateElement("name","emailAddress");
		
		type(emailAddress,email1);

		WebElement find = locateElement("xpath", "(//tbody/tr/td/em)[7]");
		click(find);
		Thread.sleep(10000);
		WebElement firstOutput = locateElement("xpath","((//tbody/tr/td[4])//preceding-sibling::td)[3]/div/a");
		getText(firstOutput);
		String text = getText(firstOutput);
		System.out.println("The Value of first element is "+text);
		WebElement firstOutput1 = locateElement("xpath","((//tbody/tr/td[4])//preceding-sibling::td)[3]/div/a");
		click(firstOutput1);

			WebElement duplicate = locateElement("linkText","Duplicate Lead");
			click(duplicate);
			
			String title = driver.getTitle();
			System.out.println(title);
			
			WebElement create = locateElement("class","smallSubmit");
			click(create);
			
WebElement nameVerify = locateElement("id","viewLead_firstName_sp");
System.out.println("First Name is "+nameVerify.getText().replaceAll("[^a-zA-Z]", ""));

	
	
	}

	@DataProvider(name="Positive")
	public Object[][] fetchData() {
		Object[][] data = new Object[2][1];
		data[0][0]="support@blahblah.com";
		data[1][0]="nadhiya@gmail.com";
		return data;
		
		
	}
	
	@DataProvider(name="Negative")
	public void fetchnegData()
	{
		
	}
	
}
