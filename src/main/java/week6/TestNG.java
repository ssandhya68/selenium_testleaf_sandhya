package week6;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TestNG extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TestNg";
		testCaseDesc="To run methods of TestNg Lead";
		category="smoke";
		author="Sandhya";
				
		
	}
	@Test(invocationCount=2)
	public void createLead()
	{
		System.out.println("create Lead");
	
	}
	@Test(enabled=false)
	public void EditLeads() throws InterruptedException {

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement findLeads = locateElement("linkText", "Find Leads");
		click(findLeads);

		WebElement name = locateElement("xpath", "((//label[text()='First name:']//parent::div/div//child::input)[3])");
		type(name,"Sandhya");

		WebElement find = locateElement("xpath", "(//tbody/tr/td/em)[7]");
		click(find);
		Thread.sleep(10000);
		WebElement firstOutput = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		getText(firstOutput);
		String text = getText(firstOutput);
		System.out.println("The Value of first element is "+text);
		WebElement firstOutput1 = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		click(firstOutput1);
		String title = driver.getTitle();
		System.out.println(title);
		WebElement edit = locateElement("linkText","Edit");
		click(edit);
		WebElement companyName = locateElement("id","updateLeadForm_companyName");
		type(companyName,"DXC Technology");
		WebElement update = locateElement("xpath","((//input[@class='smallSubmit'])[1])");
		click(update);
		Thread.sleep(10000);
		WebElement companyNameVerify = locateElement("id","viewLead_companyName_sp");
		System.out.println("Company Name is "+companyNameVerify.getText().replaceAll("[^a-zA-Z]", ""));



	}
	@Test(dependsOnMethods= {"createLead"})
	public void FindLeads() throws InterruptedException {
	
		System.out.println("Delete Lead");
		
}


}
