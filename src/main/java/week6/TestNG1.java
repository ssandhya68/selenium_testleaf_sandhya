package week6;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TestNG1 extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC002_CreateLead";
		testCaseDesc="To create Lead";
		category="smoke";
		author="Sandhya";
				
		
	}
	@Test(dataProvider="Positive")
	public void createLead(String companyName,String fname,String lname)
	{
		WebElement createLead = locateElement("linkText","Create Lead");
	click(createLead);
	
	WebElement company = locateElement("id","createLeadForm_companyName");
	type(company,companyName);
	
	WebElement firstName = locateElement("id","createLeadForm_firstName");
	type(firstName,fname);
	
	WebElement lastName = locateElement("id","createLeadForm_lastName");
	type(lastName,lname);
	
	WebElement firstNameLocal = locateElement("id","createLeadForm_firstNameLocal");
	type(firstNameLocal,"Sandhya");
	
	WebElement lastNameLocal = locateElement("id","createLeadForm_lastNameLocal");
	type(lastNameLocal,"Sundaran");
	
	WebElement salutation = locateElement("id","createLeadForm_personalTitle");
	type(salutation,"Miss");
	WebElement source = locateElement("id","createLeadForm_dataSourceId");
	selectDropDownUsingText(source,"Existing Customer");
	
	WebElement title = locateElement("id","createLeadForm_generalProfTitle");
	type(title,"Tester");
	
	WebElement annualRevenue = locateElement("id","createLeadForm_annualRevenue");
	type(annualRevenue,"1000000");
	
	WebElement industry = locateElement("id","createLeadForm_industryEnumId");
	selectDropDownUsingText(industry,"Telecommunications");
	
	WebElement ownership = locateElement("id","createLeadForm_industryEnumId");
	selectDropDownUsingText(ownership,"Public Corporation");
	
	WebElement currency = locateElement("id","createLeadForm_currencyUomId");
	selectDropDownUsingText(currency,"USD - American Dollar");
	
	
	WebElement SIC = locateElement("id","createLeadForm_sicCode");
	type(SIC,"45678");
	
	
	WebElement desc = locateElement("id","createLeadForm_description");
	type(desc,"Industry Description will be entered here");
	
	WebElement impNote = locateElement("id","createLeadForm_importantNote");
	type(impNote,"Industry Description important note to be entered here");
	
	

	
	WebElement countryCode = locateElement("id","createLeadForm_primaryPhoneCountryCode");
	countryCode.clear();
	
	type(countryCode,"+91");
	
	WebElement areaCode = locateElement("id","createLeadForm_primaryPhoneCountryCode");
	
	
	type(areaCode,"44");
	
	WebElement ext = locateElement("id","createLeadForm_primaryPhoneExtension");
			
	type(ext,"66733572");
	
	
	WebElement phone = locateElement("id","createLeadForm_primaryPhoneNumber");
	
	type(phone,"7708331227");
	
WebElement eNum = locateElement("id","createLeadForm_numberEmployees");
	
	type(eNum,"4000");
	
WebElement ticker = locateElement("id","createLeadForm_tickerSymbol");
	
	type(ticker,"%");
	
WebElement poc = locateElement("id","createLeadForm_primaryPhoneAskForName");
	
	type(poc,"Rahul");
	
WebElement url = locateElement("id","createLeadForm_primaryWebUrl");
	
	type(url,"http://www.myntra.com");

WebElement toName = locateElement("id","createLeadForm_generalToName");
	
	type(toName,"Sandhya");

WebElement address1 = locateElement("id","createLeadForm_generalAddress1");
	
	type(address1,"No.10/22");

WebElement address2= locateElement("id","createLeadForm_generalAddress2");
	
	type(address2,"10 downing street");
	
WebElement city= locateElement("id","createLeadForm_generalCity");
	
	type(city,"London");	
	
	WebElement state = locateElement("id","createLeadForm_generalStateProvinceGeoId");
	selectDropDownUsingText(state,"Wisconsin");
	
	
	
	WebElement country = locateElement("id","createLeadForm_generalCountryGeoId");
	selectDropDownUsingText(country,"United States");
	
WebElement zip= locateElement("id","createLeadForm_generalPostalCode");
	
	type(zip,"644444");

WebElement zipExt= locateElement("id","createLeadForm_generalPostalCodeExt");
	
	type(zipExt,"44");

	WebElement marketing = locateElement("id","createLeadForm_marketingCampaignId");
	selectDropDownUsingText(marketing,"Pay Per Click Advertising");
	
WebElement email= locateElement("id","createLeadForm_primaryEmail");
	
	type(email,"support@blahblah.com");
	
	WebElement createLeadSubmit = locateElement("class","smallSubmit");
	click(createLeadSubmit);
	WebElement firstNameverify = locateElement("id","viewLead_firstName_sp");
	verifyExactText(firstNameverify,"Sandhya");
	
	}
	
	
	@DataProvider(name="Positive")
	public Object[][] fetchData() {
		Object[][] data = new Object[2][3];
		data[0][0]="testleaf";
		data[0][1]="Rahul";
		data[0][2]="Munjal";
		data[1][0]="DXC";
		data[1][1]="Sandhya";
		data[1][2]="Sundaran";
		return data;
		
		
	}
	
	@DataProvider(name="Negative")
	public void fetchnegData()
	{
		
	}
	
}
