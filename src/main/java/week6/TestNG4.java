package week6;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TestNG4 extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testCaseDesc="To Update Lead";
		category="Smoke";
		author="Sandhya";
	}
	@Test(dataProvider="Positive")
	public void EditLeads(String name1,String company) throws InterruptedException {

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement findLeads = locateElement("linkText", "Find Leads");
		click(findLeads);

		WebElement name = locateElement("xpath", "((//label[text()='First name:']//parent::div/div//child::input)[3])");
		type(name,name1);

		WebElement find = locateElement("xpath", "(//tbody/tr/td/em)[7]");
		click(find);
		Thread.sleep(10000);
		WebElement firstOutput = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		getText(firstOutput);
		String text = getText(firstOutput);
		System.out.println("ThValue of first element is "+text);
		WebElement firstOutput1 = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		click(firstOutput1);
		String title = driver.getTitle();
		System.out.println(title);
		WebElement edit = locateElement("linkText","Edit");
		click(edit);
		WebElement companyName = locateElement("id","updateLeadForm_companyName");
		type(companyName,company);
		WebElement update = locateElement("xpath","((//input[@class='smallSubmit'])[1])");
		click(update);
		Thread.sleep(10000);
		WebElement companyNameVerify = locateElement("id","viewLead_companyName_sp");
		System.out.println("Company Name is "+companyNameVerify.getText().replaceAll("[^a-zA-Z]", ""));



	}
	@DataProvider(name="Positive")
	public Object[][] fetchData() {
		Object[][] data = new Object[1][2];
		data[0][0]="Sandhya";
		data[0][1]="CTS";
		return data;
		
		
	}
	
	@DataProvider(name="Negative")
	public void fetchnegData()
	{
		
	}
}
