package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class viewLead extends ProjectMethods {

	public viewLead() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(id="viewLead_firstName_sp")
	WebElement verifyFirstName;
	
	public viewLead verifyFirstName(String data) {
		//WebElement verifyFirstName = locateElement("id","viewLead_firstName_sp");
		verifyExactText(verifyFirstName,data);
		return this;
	}
	
	@FindBy(id="viewLead_lastName_sp")
	WebElement verifyLastName;
	public viewLead verifyLastName(String data) {
		//WebElement verifyLastName = locateElement("id","viewLead_lastName_sp");
		verifyExactText(verifyLastName,data);
		return this;
	}
	
	@FindBy(id="viewLeadForm_companyName_sp")
	WebElement verifyCompanyName;
	public viewLead verifyCompanyName(String data) {
		//WebElement verifyCompanyName = locateElement("id","viewLeadForm_companyName_sp");
		verifyExactText(verifyCompanyName,data);
		return this;
	}
	
}
