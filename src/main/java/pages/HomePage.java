package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		
		PageFactory.initElements(driver, this);
			}
	
	@FindBy(xpath="(//h2[text()='Demo Sales Manager'])")
	WebElement vName;
		public HomePage verifyLoggedInName(String expectedText)
		{
			//WebElement vName = locateElement("xpath","(//h2[text()='Demo Sales Manager'])");
			verifyPartialText(vName,expectedText);
			return this;
		}
		@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
		WebElement elelogout;
		public LoginPage clickLogout()
		{
			//WebElement elelogout = locateElement("class","decorativeSubmit");
			click(elelogout);
			return new LoginPage();
		}
		@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
		WebElement eleCRM;
		public MyHome clickCRM()
		{
			//WebElement eleCRM = locateElement("linkText","CRM/SFA");
			click(eleCRM);
			return new MyHome();
		}
	}


