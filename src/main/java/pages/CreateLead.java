package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement typeCompanyName;
	public CreateLead typeCompanyName(String data) {
		//WebElement typeCompanyName = locateElement("id","createLeadForm_lastName");
		type(typeCompanyName,data);
		return this;
	}
	
	@FindBy(id="createLeadForm_firstName")
	WebElement typeFirstName;
	public CreateLead typeFirstName(String data) {
		//WebElement typeFirstName = locateElement("id","createLeadForm_firstName");
		type(typeFirstName,data);
		return this;
	}
	
	@FindBy(id="createLeadForm_lastName")
	WebElement typeLastName;
	public CreateLead typeLastName(String data) {
		//WebElement typeLastName = locateElement("id","createLeadForm_lastName");
		type(typeLastName,data);
		return this;
	}
	
	@FindBy(how=How.CLASS_NAME,using="smallSubmit")
	WebElement clickCreateLead;
	public viewLead clickCreateLead() {
		//WebElement clickCreateLead = locateElement("class","smallSubmit");
		click(clickCreateLead);
		return new viewLead();
	}
	
	
}
